from typing import List, Optional
from enum import Enum

from pytos2.models import Jsonable
from pytos2.utils import propify, prop


@propify
class SlimRule(Jsonable):
    class XsiType(Enum):
        SLIM_RULE_WITH_METADATA = "ns3:slimRuleWithMetadataDTO"

    class Prop(Enum):
        RULE_NUMBER = "ruleNumber"
        SOURCE_NETWORKS = "sourceNetworks"
        DESTINATION_NETWORKS = "destinationNetworks"
        DESTINATION_SERVICES = "destinationServices"

    uid: str = prop("")
    is_disabled: bool = prop(False)
    rule_number: int = prop(0, key=Prop.RULE_NUMBER.value)
    source_networks: List[dict] = prop(factory=list, key=Prop.SOURCE_NETWORKS.value)
    destination_networks: List[dict] = prop(
        factory=list, key=Prop.DESTINATION_NETWORKS.value
    )
    destination_services: List[dict] = prop(
        factory=list, key=Prop.DESTINATION_SERVICES.value
    )
    track: Optional[dict] = prop(None)
    install_ons: List[dict] = prop(factory=list)
    communities: List[dict] = prop(factory=list)
    times: List[dict] = prop(factory=list)


@propify
class SlimRuleMetadata(Jsonable):
    permisssiveness_level: str = prop("")
    violations: List[str] = prop(
        factory=list
    )
    last_hit: str = prop("")
    shadowed_status: str = prop("")
    ticket_ids: List[int] = prop(factory=list)
    business_owners: List[str] = prop(factory=list)
    expirations: List[str] = prop(factory=list)
    applications: List[str] = prop(factory=list)
    last_modified: str = prop("")


@propify
class SlimRuleWithMetadata(SlimRule):
    rule_metadata: Optional[SlimRuleMetadata] = prop(None)
